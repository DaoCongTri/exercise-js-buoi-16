// BT1
document.getElementById("submit-bt1").onclick = function(){
    var S = 0;
    var n = 0;
    while (S < 10000) {
        n++;
        S += n;
    }
    document.getElementById("result-bt1").innerHTML = `
        <h3>Số nguyên dương nhỏ nhất: ${n}</h3>
    `;

}
// BT2
document.getElementById("submit-bt2").addEventListener("click", function(){
    var x = parseFloat(document.getElementById("bt2-x").value);
    var n = parseInt(document.getElementById("bt2-n").value);
    var sum = TinhTongBT2(x,n);
    document.getElementById("result-bt2").innerHTML =`
        <h3>Tổng của biểu thức S = ${sum}</h3>
    `;
});
function TinhTongBT2(x,n){
    var T = 1;
    var S = 0;
    for(var i = 1; i <= n; i++){
        T = T * x;
        S = S + T;
    }
    return S;
    // có thể sử dụng vòng lặp while để tính biểu thức
    /*while(i <= n){
        T = T * x;
        S = S + T;
        i++;
    }*/
}
// BT3
document.getElementById("submit-bt3").onclick = function(){
    var bt3 = document.getElementById("number-bt3").value * 1;
    var giaithua = 1;
    for(var i = 1; i <= bt3; i++){
        giaithua *= i;
    }
    document.getElementById("result-bt3").innerHTML = `
        <h3>Giai thừa của số n: ${giaithua}</h3>
    `;
}
// BT4
document.getElementById("submit-bt4").onclick = function(){
    var divBT4 = document.getElementById("div-bt4");
    divBT4.style.display = 'block';
    divBT4.style.color = 'white';
   var TagDiv = document.querySelectorAll("#bt4");
   for (var i = 0; i < TagDiv.length; i++){
        TagDiv[i].className = 'h3';
        // Vị trí chẵn => màu đỏ
        if ((i + 1) % 2 == 0){
            TagDiv[i].style.background = "red";
        }
        else { // Vị trí lẽ => màu xanh
            TagDiv[i].style.background = "blue";
        }
    }
}
